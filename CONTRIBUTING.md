# Wie kann ich mich einbringen?

## Ich bin THWler und interessiere mich für TaKa
Schön das du hier bist!
Es gibt verschiedene Wege wie du das Projekt unterstützen kannst.
Zum einen natürlich in dem mit am SourceCode schreibst, dazu im nächsten Abschnitt mehr.
Du kannst oder möchtest nicht mit coden?
Kein Problem auch für dich gibt es Möglichkeiten wie du dich einbringen kannst.
Am einfachsten trittst du dem Channel `A_THW_Entwicklung-TaKa-APP` bei und gibst Feedback wie dir die APP gefällt oder was dir noch fehlt.
Wir möchten auch mittelfristig das ihr selber Karten erstellen und sie auch anderen zur Verfügung stellen könnt.

## Ich bin THWler und möchte mitcoden oder mich an anderer Stelle in der Entwicklung einrbingen?
Schön das du hier bist!
Wir sammeln aktuell noch Anforderungen und möchten dann entscheiden wie wir das Projekt aufsetzen.
Ganz grundsätzlich kannst du dazu aber auch schon deine Gedanken freien Lauf lassen entweder hier oder im Hermine Channel.
Auch diesen Contribution Guide wollen wir noch erweitern um den Einstieg einfacher zu machen.
Bitte stelle doch eine Anfrage als Entwickler für diese Gruppe in Gitlab: https://gitlab.com/thw2/taschenkarten
Dann können wir dir bereits Zugriff geben und sobald der erste Code geschrieben wird kannst du direkt mit Hand anlegen.
Schreibe doch zudem bitte eine kurze Beschreibung was du kannst und was du gerne machen würdest in den Hermine Channel.

### Mitschreiben der Dokumente in diesem Repo
Fühl dich frei deine Gedanken hier in die Dokumente einfließen zu lassen.
Wenn du die Dokumente editiert hast stelle bitte einen PR damit wir deine Änderungen übernehmen können.
Solltet ihr an manchen Stellen anderer Meinung sein, schreibt das einfach in Hermine oder reicht direkt einen PR ein dann kann man darüber sprechen.
Hier sollen alle, die sich hier zuverlässig einbringen, auch die Rechte bekommen den PR einer anderen Person zu mergen.
