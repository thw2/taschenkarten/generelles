# mögliche Implementierungen

## Frontends
Um die Menge an Code so gering wie möglich zu halten sollte man den Frontend übergreifenden Code in Kotlin schreiben und dann über Cross Compile wiederverwenden.
Desweiteren würde ich die Frontends offline fähig machen und lediglich als Darstellung nutzen.

## Backend
Da wir verschiedene Frontends beliefern möchten werden wir vermutlich ein schmales Backend benötigen das die Daten initial und bei Updates ausliefert
*Frage:* Wollen wir von Anfang an einen Server anbieten, welcher Datenupdates ausliefert? Können wir das nicht zunächst (zumidnest für v1) über die Stores machen?
Dann sparen wir uns am Anfang die Investion in eine Serverapplikation, welche auch entsprechend abgesichert und gepflegt werden muss.
Natürlich ist es nicht die schönste Lösung.

## Datenstruktur
Hier kommt der tricky Part.
Generell wäre es schön ein einfaches bzw. mögliches einheitliches Format zu haben um die Karten einfach in der Erstellung und Updates zu halten.
Eventuell ist es hier sogar möglich die Dokumente der Leitung direkt zu nutzen.
Allerdings müsste man hier die Rechtslage klären und sich ein OK einholen.
Möchte jemand die Aufgabe übernehmen?
Dann einfach auf die Todo Liste in der Readme setzen und vlt deinen Namen dahinter packen oder in Hermine schreiben.

Eine PowerPoint-Vorlage für Taschenkarten gibt es im Taschenkartenchannel.

### Format der Daten
Aktuell liegen die meisten Taschenkarten als PDF vor.
Daher wäre es naheliegend dieses Format ebenfalls zu nutzen.
Nachteile wären hier aber, dass die PDFs in den meisten Fällen zwei Taschenkarten enthalten.
Zudem könnte man nicht einfach nur eine Seite der Taschenkarte anzeigen.
Daher liegt es nahe lieber ein Format zu wählen, welches sich einfach einbinden lässt.
Um es anderen Personen leicht zu machen selbst Karten zu erstellen und einzufügen macht es Sinn ein Bild Format zu wählen.
Hier muss man sich auch nicht zwingend auf ein bestimmtes festzulegen. 
Dazu könnte man einen Variationspunkt in die Software einbauen und ein Interface bereit zustellen.
Die Implementierung könnte dann selber über die Darstellung entscheiden und man könnte später weitere Varianten implementieren.

### Ablage der Daten
Man könnte die Daten in einer Ordnerstruktur ablegen und die Ordnernamen nutzen um den Baum der Inhalte aufzubauen.

Wäre es möglich, dass wir die Taschenkarten als Markdowndateien in einer Ordnerstruktur ablegen? Wir könnten in diesen Dateien Bilder direkt referenzieren.
Der Vorteil von md wäre, dass wir verfügbare Bibliotheken zur Interpretierung und ggf. auch zur Darstellung und Bearbeitung nutzen könnten.
Andererseits müssten wir bestehende Taschenkarten von PDF zu md konvertieren.