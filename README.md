# ReadMe

Diese Projekt soll die aktuelle TaKa App auf iOS und ins Web bringen.
Sowie eventuell weitere gewünschte Features mitbringen.
Es gibt einen Hermine Channel um weitere Features etc. zu sammeln und um zu diskutieren: A_THW_Entwicklung-TaKa-APP

## Zielgruppe
*Frage:* Wer ist unsere Zielgruppe?

## Ideen / Wünsche
* weitere Frontends
    * iOS
    * Web
* Offline Verfügbarkeit
* Druckmöglichkeit
* Möglichkeit zur einfachen Taschenkartenerstellung für Laien
* Teilen der selbst erstellten Taschenkarten mit anderen Benutzern (global oder kann die Nutzergruppe eingeschränkt werden?)
* *Frage:* Wer soll alles Zugriff haben (nur THW-Angehörige oder egal wer?)

## Roadmap
### v1.0.0
* [ ] Anforderungen sammeln
* [ ] Opensource Programm verlängern
  * wurde bereits angestoßen
* [ ] Ein weiteres Frontend (iOS oder Web)
* [ ] Offline Verfügbarkeit

### v2.0.0
* Druckmöglichkeit
* hinzufügen eigener Taschenkarten

### v3.0.0
* Teilen eigener Taschenkarten *(oder schon in v3.0.0?)*

## Contributing
Sie hierzu contributing.md

## Authors and acknowledgment
Daniel Pfeil <opensource@danis-bu.de>
und viele weitere (fühlt euch frei bei eurem ersten Commit euren Namen hier einzutragen)
Hier geht auch ein herzlicher Dank raus an alle die uns im Channel unterstützen oder in anderer Form am Projekt teilhaben!
Gerne fügen wir auch euren Namen hier ein, sprecht uns dazu einfach am besten auf Hermine an.